const express = require('express');
const EventsController = require('../controllers/events.controller');
const authAdminMiddleware = require('../middlewares/authAdmin.middleware');
const schemaValidationMiddleware = require('../middlewares/schemaValidation.middleware');
const { eventPostSchema, eventPutSchema } = require('../../data/schemas/event.schema');

const router = express.Router();

router.post('/', schemaValidationMiddleware(eventPostSchema), authAdminMiddleware, EventsController.post);
router.put('/:id', schemaValidationMiddleware(eventPutSchema), authAdminMiddleware, EventsController.put);

module.exports = router;
