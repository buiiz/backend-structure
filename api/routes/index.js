const betsRoutes = require('./bets.routes');
const eventsRoutes = require('./events.routes');
const healthRoutes = require('./health.routes');
const statsRoutes = require('./stats.routes');
const transactionsRoutes = require('./transactions.routes');
const usersRoutes = require('./users.routes');

const errorMiddleware = require('../middlewares/error.middleware');

module.exports = (app) => {
  app.use('/bets', betsRoutes);
  app.use('/events', eventsRoutes);
  app.use('/health', healthRoutes);
  app.use('/stats', statsRoutes);
  app.use('/transactions', transactionsRoutes);
  app.use('/users', usersRoutes);
  app.use(errorMiddleware);
};
