const express = require('express');
const TransactionsController = require('../controllers/transactions.controller');
const authAdminMiddleware = require('../middlewares/authAdmin.middleware');
const schemaValidationMiddleware = require('../middlewares/schemaValidation.middleware');
const { transactionPostSchema } = require('../../data/schemas/transaction.schema');

const router = express.Router();

router.post('/', schemaValidationMiddleware(transactionPostSchema), authAdminMiddleware, TransactionsController.post);

module.exports = router;
