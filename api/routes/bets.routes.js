const express = require('express');
const BetsController = require('../controllers/bets.controller');
const authMiddleware = require('../middlewares/auth.middleware');
const schemaValidationMiddleware = require('../middlewares/schemaValidation.middleware');
const { betsPostSchema } = require('../../data/schemas/bets.schema');

const router = express.Router();

router.post('/', schemaValidationMiddleware(betsPostSchema), authMiddleware, BetsController.post);

module.exports = router;
