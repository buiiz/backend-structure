const express = require('express');
const UsersController = require('../controllers/users.controller');
const authMiddleware = require('../middlewares/auth.middleware');
const schemaValidationMiddleware = require('../middlewares/schemaValidation.middleware');
const { userGetSchema, userPostSchema, userPutSchema } = require('../../data/schemas/user.schema');

const router = express.Router();

router.get('/:id', schemaValidationMiddleware(userGetSchema), UsersController.get);
router.post('/', schemaValidationMiddleware(userPostSchema), UsersController.post);
router.put('/:id', schemaValidationMiddleware(userPutSchema), authMiddleware, UsersController.put);

module.exports = router;
