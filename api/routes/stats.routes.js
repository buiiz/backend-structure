const express = require('express');
const StatsController = require('../controllers/stats.controller');
const authAdminMiddleware = require('../middlewares/authAdmin.middleware');

const router = express.Router();

router.get('/', authAdminMiddleware, StatsController.get);

module.exports = router;
