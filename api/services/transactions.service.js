const TransactionRepository = require('../../data/repositories/transaction.repository');
const { keysToCamel, keysToSnake } = require('../../helpers/case.helper');

class TransactionsService {
  async create(data) {
    const result = await TransactionRepository.create(keysToSnake(data));
    return keysToCamel(result);
  }
}

module.exports = new TransactionsService();
