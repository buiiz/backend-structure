const OddsRepository = require('../../data/repositories/odds.repository');

class OddsService {
  async create(data) {
    const result = await OddsRepository.create(data);
    return result;
  }
}

module.exports = new OddsService();
