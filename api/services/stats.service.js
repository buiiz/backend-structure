const BetRepository = require('../../data/repositories/bet.repository');
const EventRepository = require('../../data/repositories/event.repository');
const UserRepository = require('../../data/repositories/user.repository');

class StatsService {
  async getStats() {
    const bets = await BetRepository.getAll();
    const events = await EventRepository.getAll();
    const users = await UserRepository.getAll();

    return {
      totalBets: bets.length,
      totalEvents: events.length,
      totalUsers: users.length,
    };
  }
}

module.exports = new StatsService();
