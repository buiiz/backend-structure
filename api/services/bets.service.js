const BetsRepository = require('../../data/repositories/bet.repository');

class BetsService {
  async update(id, data) {
    const result = await BetsRepository.update(id, data);
    return result;
  }

  async getByEventId(id) {
    const result = await BetsRepository.getByEventId(id);
    return result;
  }
}

module.exports = new BetsService();
