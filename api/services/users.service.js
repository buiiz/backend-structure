const UserRepository = require('../../data/repositories/user.repository');

class UsersService {
  async getAll() {
    const result = UserRepository.getAll();
    return result;
  }

  async getOne(id) {
    const result = UserRepository.getOne(id);
    return result;
  }

  async create(data) {
    const result = await UserRepository.create(data);
    return result;
  }

  async update(id, data) {
    const result = await UserRepository.update(id, data);
    return result;
  }
}

module.exports = new UsersService();
