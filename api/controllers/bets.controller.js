const createError = require('http-errors');
const UserService = require('../services/users.service');
const EventsService = require('../services/events.service');
const BetsService = require('../services/bets.service');
const OddsService = require('../services/odds.service');
const { keysToCamel, keysToSnake } = require('../../helpers/case.helper');

class BetsController {
  async post(req, res, next) {
    try {
      const userId = req.tokenPayload.id;
      req.body.user_id = userId;
      req.body = keysToSnake(req.body);

      const user = await UserService.getOne(userId);
      if (!user) {
        throw new createError.BadRequest('User does not exist');
      }
      if (+user.balance < +req.body.bet_amount) {
        throw new createError.BadRequest('Not enough balance');
      }

      const event = await EventsService.getOne('id', req.body.event_id);
      if (!event) {
        throw new createError.NotFound('Event not found');
      }

      const odds = await OddsService.getOne('id', event.odds_id);
      if (!odds) {
        throw new createError.NotFound('Odds not found');
      }

      let multiplier;
      switch (req.body.prediction) {
        case 'w1':
          multiplier = odds.home_win;
          break;
        case 'w2':
          multiplier = odds.away_win;
          break;
        case 'x':
          multiplier = odds.draw;
          break;
        default:
          break;
      }

      const bet = await BetsService.create({ ...req.body, multiplier, event_id: event.id });
      const currentBalance = user.balance - req.body.bet_amount;
      const updatedUser = await UserService.update(userId, { balance: currentBalance });

      return res.send({ ...keysToCamel(bet), currentBalance });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new BetsController();
