const StatsService = require('../services/stats.service');

class StatsController {
  async get(_req, res, next) {
    try {
      const stats = await StatsService.getStats();
      res.send(stats);
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new StatsController();
