const OddsService = require('../services/odds.service');
const EventsService = require('../services/events.service');
const BetsService = require('../services/bets.service');
const UsersService = require('../services/users.service');
const { keysToCamel, keysToSnake } = require('../../helpers/case.helper');

class EventsController {
  async post(req, res, next) {
    try {
      req.body.odds = keysToSnake(req.body.odds);
      let odds = await OddsService.create(req.body.odds);
      req.body = keysToSnake(req.body);
      const event = await EventsService.create({ ...req.body, odds_id: odds.id });
      odds = keysToCamel(odds);
      res.send({ ...event, odds });
    } catch (err) {
      next(err);
    }
  }

  async put(req, res, next) {
    try {
      const eventId = req.params.id;
      const bets = await BetsService.getByEventId(eventId);

      const [w1, w2] = req.body.score.split(':');
      let result;
      if (+w1 > +w2) {
        result = 'w1';
      } else if (+w2 > +w1) {
        result = 'w2';
      } else {
        result = 'x';
      }

      const event = await EventsService.update(eventId, { score: req.body.score });

      Promise.all(
        bets.map(async (bet) => {
          if (bet.prediction === result) {
            const updatedBet = await BetsService.update(bet.id, { win: true });
            const user = await UsersService.getOne(bet.user_id);
            const balance = user.balance + bet.bet_amount * bet.multiplier;
            const updatedUser = await UsersService.update({ balance });
          } else {
            const updatedBet = await BetsService.update(bet.id, { win: false });
          }
        })
      );

      res.send(keysToCamel(event));
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new EventsController();
