const createError = require('http-errors');
const TransactionsService = require('../services/transactions.service');
const UserService = require('../services/users.service');
const { keysToCamel, keysToSnake } = require('../../helpers/case.helper');

class TransactionsController {
  async post(req, res, next) {
    try {
      let data = req.body;

      const user = await UserService.getOne(data.userId);
      if (!user) {
        throw new createError.BadRequest('User does not exist');
      }

      data = keysToSnake(data);

      let transaction = await TransactionsService.create(data);
      const currentBalance = data.amount + user.balance;
      const newUser = await UserService.update(data.user_id, { balance: currentBalance });

      transaction = keysToCamel(transaction);

      return res.send({ ...transaction, currentBalance });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new TransactionsController();
