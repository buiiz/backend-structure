const createError = require('http-errors');
const UserService = require('../services/users.service');
const { generateToken } = require('../../helpers/jwt.helper');
const { keysToCamel } = require('../../helpers/case.helper');

class UsersController {
  async get(req, res, next) {
    try {
      const result = await UserService.getOne(req.params.id);
      if (!result) {
        throw new createError.NotFound('User not found');
      }
      res.send({ ...result });
    } catch (err) {
      next(err);
    }
  }

  async post(req, res, next) {
    try {
      let result = await UserService.create({ ...req.body, balance: 0 });
      result = keysToCamel(result);
      const accessToken = generateToken({ id: result.id, type: result.type });
      res.send({ ...result, accessToken });
    } catch (err) {
      next(err);
    }
  }

  async put(req, res, next) {
    try {
      if (req.params.id !== req.tokenPayload.id) {
        throw new createError.Unauthorized('UserId mismatch');
      }
      const result = await UserService.update(req.params.id, req.body);
      res.send({ ...result });
    } catch (err) {
      next(err);
    }
  }
}

module.exports = new UsersController();
