const createError = require('http-errors');

const schemaValidation = (schema) => (req, res, next) => {
  try {
    const data = req.method === 'GET' ? req.params : req.body;
    const isValidResult = schema.validate(data);

    if (isValidResult?.error) {
      throw new createError.BadRequest(isValidResult.error.details[0].message);
    }

    next();
  } catch (err) {
    next(err);
  }
};

module.exports = schemaValidation;
