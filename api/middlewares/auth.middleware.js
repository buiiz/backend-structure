const createError = require('http-errors');
const { validateToken } = require('../../helpers/jwt.helper');

const authMiddleware = (req, _res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw new createError.Unauthorized('Not Authorized');
    }

    const [type, token] = authHeader.split(' ');
    if (type !== 'Bearer' || !token) {
      throw new createError.Unauthorized('Not Authorized');
    }

    const decoded = validateToken(token);
    if (!decoded) {
      throw new createError.Unauthorized('Not Authorized');
    }

    req.tokenPayload = decoded;

    return next();
  } catch (err) {
    return next(err);
  }
};

module.exports = authMiddleware;
