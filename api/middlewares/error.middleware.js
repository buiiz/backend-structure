const { StatusCodes, getReasonPhrase } = require('http-status-codes');
const { Logger } = require('../../logger');

const errorMiddleware = (err, _req, res, next) => {
  console.log(err);

  if (err.code === '23505') {
    Logger.info({ name: StatusCodes.BAD_REQUEST, message: err.detail });
    return res.status(StatusCodes.BAD_REQUEST).send({ error: err.detail });
  }

  if (err) {
    const statusCode = err.status || '500';
    const reasonPhrase = err.message || getReasonPhrase(statusCode);

    Logger.info({ name: statusCode, message: reasonPhrase });
    return res.status(statusCode).send({ error: reasonPhrase });
  }

  next();
};

module.exports = errorMiddleware;
