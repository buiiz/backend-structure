const createError = require('http-errors');
const { validateToken } = require('../../helpers/jwt.helper');

const authAdminMiddleware = (req, _res, next) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw new createError.Unauthorized('Not Authorized');
    }

    const [type, token] = authHeader.split(' ');
    if (type !== 'Bearer' || !token) {
      throw new createError.Unauthorized('Not Authorized');
    }

    const decoded = validateToken(token);
    if (!decoded || decoded.type !== 'admin') {
      throw new createError.Unauthorized('Not Authorized');
    }

    req.tokenPayload = decoded;

    next();
  } catch (err) {
    next(err);
  }
};

module.exports = authAdminMiddleware;
