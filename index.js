const express = require('express');
const morgan = require('morgan');
const { port } = require('./config/config');
const db = require('./data/db/connection');
const routes = require('./api/routes');
const { stream, Logger } = require('./logger');

const app = express();

app.use(express.json());
app.use(morgan('combined', { stream }));
app.use((_req, _res, next) => {
  db.raw('select 1+1 as result')
    .then(() => next())
    .catch((err) => {
      Logger.error('Unable to connect to the database');
      process.exit(1);
    });
});

routes(app);

process
  .on('unhandledRejection', (err) => {
    Logger.error({ name: 'unhandledRejection', message: err.message });
  })
  .on('uncaughtException', (err) => {
    Logger.error({ name: 'uncaughtException', message: err.message });
    Logger.on('finish', () => process.exit(1));
  });

app.listen(port, () => console.log(`App listening at http://localhost:${port}`));

module.exports = { app };
