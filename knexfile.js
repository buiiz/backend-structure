const config = require('./config/knex.config');

module.exports = {
  development: {
    client: 'postgresql',
    connection: {
      port: config.port,
      host: config.host,
      database: config.name,
      user: config.user,
      password: config.password,
    },
    migrations: {
      directory: './data/migrations',
      tableName: 'knex_migrations',
    },
    seeds: {
      directory: './data/seeds',
    },
    pool: {
      min: 0,
      max: 20,
    },
  },
};
