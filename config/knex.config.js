require('dotenv').config();

const config = {
  port: process.env.DATABASE_PORT || 3000,
  user: process.env.DATABASE_USER,
  password: process.env.DATABASE_ACCESS_KEY,
  host: process.env.DATABASE_HOST,
  name: process.env.DATABASE_NAME,
};

module.exports = config;
