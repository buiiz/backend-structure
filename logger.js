const winston = require('winston');

const { combine, timestamp, colorize, json, cli } = winston.format;

const Logger = winston.createLogger({
  exitOnError: false,
  format: combine(timestamp(), json()),
  transports: [
    new winston.transports.File({ filename: './logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: './logs/combined.log' }),
    new winston.transports.Console({
      format: combine(cli(), colorize()),
    }),
  ],
});

const stream = {
  write: (text) => {
    Logger.info(text);
  },
};

module.exports = { Logger, stream };
