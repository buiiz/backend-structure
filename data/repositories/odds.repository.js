const BaseRepository = require('./base.repository');
const Odds = require('../models/odds.model');

class OddsRepository extends BaseRepository {
  constructor() {
    super('odds', Odds);
  }
}

module.exports = new OddsRepository();
