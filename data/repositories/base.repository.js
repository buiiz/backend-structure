const db = require('../db/connection');

class BaseRepository {
  constructor(name, model) {
    this.db = db(name);
    this.Model = model;
  }

  create(data) {
    return this.db
      .insert({ ...data })
      .returning('*')
      .then(([result]) => result);
  }

  update(id, data) {
    return this.db
      .where({ id })
      .update(data)
      .returning('*')
      .then(([result]) => result);
  }

  delete(id) {
    return this.db
      .where({ id })
      .del()
      .then(([result]) => result);
  }

  getAll() {
    return this.db.select();
  }

  getOne(id) {
    return this.db
      .where({ id })
      .returning('*')
      .then(([result]) => result);
  }
}

module.exports = BaseRepository;
