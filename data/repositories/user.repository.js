const BaseRepository = require('./base.repository');
const User = require('../models/user.model');

class UsersRepository extends BaseRepository {
  constructor() {
    super('user', User);
  }
}

module.exports = new UsersRepository();
