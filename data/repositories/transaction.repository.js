const BaseRepository = require('./base.repository');
const Transaction = require('../models/transaction.model');

class TransactionRepository extends BaseRepository {
  constructor() {
    super('transaction', Transaction);
  }
}

module.exports = new TransactionRepository();
