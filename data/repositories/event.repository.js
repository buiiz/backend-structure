const BaseRepository = require('./base.repository');
const Event = require('../models/event.model');

class EventRepository extends BaseRepository {
  constructor() {
    super('event', Event);
  }
}

module.exports = new EventRepository();
