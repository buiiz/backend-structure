const BaseRepository = require('./base.repository');
const Bet = require('../models/bet.model');

class BetRepository extends BaseRepository {
  constructor() {
    super('bet', Bet);
  }

  getByEventId(id) {
    return this.db
      .where('event_id', id)
      .andWhere('win', null)
      .then((result) => result);
  }
}

module.exports = new BetRepository();
