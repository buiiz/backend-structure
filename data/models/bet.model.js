class Bet {
  constructor(data) {
    this.id = data.id;
    this.eventId = data.eventId;
    this.userId = data.userId;
    this.betAmount = data.betAmount;
    this.prediction = data.prediction;
    this.multiplier = data.multiplier;
    this.win = data.win;
  }
}

module.exports = Bet;
