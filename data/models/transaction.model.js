class Transaction {
  constructor(data) {
    this.id = data.id;
    this.userId = data.userId;
    this.cardNumber = data.cardNumber;
    this.amount = data.amount;
  }
}

module.exports = Transaction;
