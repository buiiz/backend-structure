const { v4 } = require('uuid');

class Odds {
  constructor({ id = v4(), homeWin = 0, draw = 0, awayWin = 0 }) {
    this.id = id;
    this.homeWin = homeWin;
    this.draw = draw;
    this.awayWin = awayWin;
  }
}

module.exports = Odds;
