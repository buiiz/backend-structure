class Event {
  constructor(data) {
    this.id = data.id;
    this.oddsId = data.oddsId;
    this.type = data.type;
    this.homeTeam = data.homeTeam;
    this.awayTeam = data.awayTeam;
    this.score = data.score;
    this.startAt = data.startAt;
  }
}

module.exports = Event;
