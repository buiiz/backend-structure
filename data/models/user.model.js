const { v4 } = require('uuid');

class User {
  constructor({
    id = v4(),
    type = 'TYPE',
    email = 'EMAIL',
    phone = 'PHONE',
    name = 'NAME',
    balance = 0,
    city = 'CITY',
    password = 'PASSWORD',
    createdAt = new Date().toISOString(),
    updatedAt = null,
  }) {
    this.id = id;
    this.type = type;
    this.email = email;
    this.phone = phone;
    this.this.name = name;
    this.city = city;
    this.balance = balance;
    this.password = password;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }
}

module.exports = User;
