const isObject = (obj) => obj === Object(obj) && !Array.isArray(obj) && typeof obj !== 'function';

const camelToSnakeCase = (str) => str.replace(/[A-Z]/g, (letter) => `_${letter.toLowerCase()}`);

const snakeToCamelCase = (str) => str.replace(/([_][a-z])/gi, ($1) => $1.toUpperCase().replace('_', ''));

const keysToCamel = (obj) => {
  if (isObject(obj)) {
    const newObj = {};
    Object.keys(obj).forEach((k) => {
      newObj[snakeToCamelCase(k)] = keysToCamel(obj[k]);
    });
    return newObj;
  }

  if (Array.isArray(obj)) {
    return obj.map((i) => {
      return keysToCamel(i);
    });
  }

  return obj;
};

const keysToSnake = (obj) => {
  if (isObject(obj)) {
    const newObj = {};
    Object.keys(obj).forEach((k) => {
      newObj[camelToSnakeCase(k)] = keysToSnake(obj[k]);
    });
    return newObj;
  }

  if (Array.isArray(obj)) {
    return obj.map((i) => {
      return keysToSnake(i);
    });
  }

  return obj;
};

module.exports = { keysToCamel, keysToSnake };
