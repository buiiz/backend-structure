const jwt = require('jsonwebtoken');
const { secret } = require('../config/jwt.config');

const generateToken = (payload) => {
  try {
    const token = jwt.sign(payload, secret);
    return token;
  } catch (err) {
    return null;
  }
};

const validateToken = (token) => {
  try {
    const userData = jwt.verify(token, secret);
    return userData;
  } catch (err) {
    return null;
  }
};

module.exports = { generateToken, validateToken };
